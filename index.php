<?php session_start();

 ?>
<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<title></title>
</head>
<body>
<form action="server.php" method="POST">
	<div class="login-wrap">
	<div class="login-html">
		<input id="tab-1" type="radio" name="tab" class="sign-in" checked <?php if(isset($_SESSION['signup'])){print $_SESSION['signup'];} ?>><label for="tab-1" class="tab" >Sign In</label>
		<input id="tab-2" type="radio" name="tab" class="sign-up" <?php if(isset($_SESSION['signup'])){print $_SESSION['signup'];} ?>><label for="tab-2" class="tab">Sign Up</label>
		<div class="login-form">
			<div class="sign-in-htm">
				<div class="group">
					<label for="user" class="label">email</label>
					<div><?php if(isset($_SESSION['email1_error'])){print $_SESSION['email1_error'];} ?></div>
					<input id="user" type="text" class="input" name="email1" value="<?php if(isset($_SESSION['email1'])){
				print $_SESSION['email1'];
			} ?>">
				</div>
				<div class="group">
					<label for="pass" class="label">Password</label>
					<div><?php if(isset($_SESSION['password1_error'])){print $_SESSION['password1_error'];} ?></div>
					<input id="pass" type="password" class="input" data-type="password" name="password1">
				</div>
				<div class="group">
					<input id="check" type="checkbox" class="check" checked>
					<label for="check"><span class="icon"></span> Keep me Signed in</label>
				</div>
				<div class="group">
					<input type="submit" class="button" value="Sign In" name="signin">
				</div>
				<div class="hr"></div>
				<div class="foot-lnk">
					<a href="#forgot">Forgot Password?</a>
				</div>
			</div>
				
			<div class="sign-up-htm">
				<div class="group">
					<label for="user" class="label">name</label>
					<div><?php if(isset($_SESSION['name_error'])){print $_SESSION['name_error'];} ?></div>
					<input id="user" type="text" class="input" name="name" value="<?php if(isset($_SESSION['name'])){
				print $_SESSION['name'];
			} ?>">
				</div>
				<div class="group">
					<label for="user" class="label">surname</label>
					<div><?php if(isset($_SESSION['surname_error'])){print $_SESSION['surname_error'];} ?></div>
					<input id="user" type="text" class="input" name="surname" value="<?php if(isset($_SESSION['surname'])){
				print $_SESSION['surname'];
			} ?>">
				</div>
				<div class="group">
					<div class="group">
					<label for="user" class="label">age</label>
					<div><?php if(isset($_SESSION['age_error'])){print $_SESSION['age_error'];} ?></div>
					<input id="user" type="number" class="input" name="age" value="<?php if(isset($_SESSION['age'])){
				print $_SESSION['age'];
			} ?>">
				</div>
					<label for="pass" class="label">Password</label>
					<div><?php if(isset($_SESSION['password_error'])){print $_SESSION['password_error'];} ?></div>
					<input id="pass" type="password" class="input" data-type="password" name="password" value="<?php if(isset($_SESSION['password'])){
				print $_SESSION['password'];
			} ?>">
				</div>
				<div class="group">
					<label for="pass" class="label">Repeat Password</label>
					<div><?php if(isset($_SESSION['repeat_password_error'])){print $_SESSION['repeat_password_error'];} ?></div>
					<input id="pass" type="password" class="input" data-type="password" name="repeat_password" value="<?php if(isset($_SESSION['repeat_password'])){
				print $_SESSION['repeat_password'];
			} ?>">
				</div>
				<div class="group">
					<label for="pass" class="label">Email Address</label>
					<div><?php if(isset($_SESSION['email_error'])){print $_SESSION['email_error'];} ?></div>
					<input id="pass" type="text" class="input" name="email" value="<?php if(isset($_SESSION['email'])){
				print $_SESSION['email'];
			} ?>">
				</div>
				<div class="group">
					<input type="submit" class="button" value="Sign Up" name="signup">
				</div>
				<div class="hr"></div>
				<div class="foot-lnk">
					<label for="tab-1">Already Member?</a>
				</div>
			</div>
			</form>
		</div>
	</div>
</div>
	<?php
	   unset($_SESSION['password1_error']);
       unset($_SESSION['password1']);
       unset($_SESSION['email1_error']);
       unset($_SESSION['email1']);
       unset($_SESSION['name_error']);
       unset($_SESSION['name']);
       unset($_SESSION['surname_error']);
       unset($_SESSION['surname']);
       unset($_SESSION['email_error']);
       unset($_SESSION['email']);
       unset($_SESSION['age_error']);
       unset($_SESSION['age']);
       unset($_SESSION['password_error']);
       unset($_SESSION['password']);
       unset($_SESSION['repeat_password_error']);
       unset($_SESSION['repeat_password']);
       unset($_SESSION['user']);
	?>

</body>

</html>