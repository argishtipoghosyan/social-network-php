<?php

 include 'pages/header.php' ?>

<section id="content">
	<form action="server.php" method="POST">
		<div class="sign-up-htm">
		<div class="group">
			<label class="label1">Name</label>
			<div><?php if(isset($_SESSION['up_name_error'])){print $_SESSION['up_name_error'];} ?></div>
            <input type="text" name="up_name" value="<?php if(isset($_SESSION['up_name'])){
				print $_SESSION['up_name'];
	        } else{print $user['name'] ;}?>">
	    </div>
	    <div >
			<label class="label1">Surname</label>
			<div><?php if(isset($_SESSION['up_surname_error'])){print $_SESSION['up_surname_error'];} ?></div>
        <input type="text" name="up_surname" value="<?php if(isset($_SESSION['up_surname'])){
				print $_SESSION['up_surname'];
			} else{print $user['surname'] ;}?>">
		</div>
		<div class="group">
			<label class="label1">Age</label>
			<div><?php if(isset($_SESSION['up_age_error'])){print $_SESSION['up_age_error'];} ?></div>	
        <input type="number" name="up_age" value="<?php if(isset($_SESSION['up_age'])){
				print $_SESSION['up_age'];
			} else{print $user['age'] ;}?>">
		</div>
		<div class="group">
			<label class="label1">Email</label>
			<div><?php if(isset($_SESSION['up_email_error'])){print $_SESSION['up_email_error'];} ?></div>	
        <input type="text" name="up_email" value="<?php if(isset($_SESSION['up_email'])){
				print $_SESSION['up_email'];
			} else{print $user['email'] ;}?>">
			
		</div>	
        <button type="submit" name="update" class="but_pas">Update</button>
        </div>
        <div class="updatepassword">
            <div class="group">
        	    <label class="label1">Password</label>
			    <div><?php if(isset($_SESSION['passerror'])){print $_SESSION['passerror'];} ?></div>
                <input type="password" name="password" class="pas_inp">
            </div>
            <div class="group">
                <label class="label1">Newpassword</label>    
			    <div><?php if(isset($_SESSION['passerror1'])){print $_SESSION['passerror1'];} ?></div>
                <input type="password" name="password1" class="pas_inp">
            </div>
            <div class="group">
                <label class="label1">Repead_newpassword</label>    
			    <div><?php if(isset($_SESSION['passerror2'])){print $_SESSION['passerror2'];} ?></div>
                <input type="password" name="password2" class="pas_inp">
            </div>
            <button type="submit" name="updatepas" class="but_pas">Update-Password</button>
        </div>
	</form>
</section>

<?php include 'pages/footer.php' ?>


<?php
    unset($_SESSION['up_name_error']);
    unset($_SESSION['up_name']);
    unset($_SESSION['up_surname_error']);
    unset($_SESSION['up_surname']);
    unset($_SESSION['up_age_error']);
    unset($_SESSION['up_age']);
    unset($_SESSION['up_email_error']);
    unset($_SESSION['up_email']);
    unset($_SESSION['password']);
    unset($_SESSION['passerror']);
    unset($_SESSION['password1']);
    unset($_SESSION['passerror1']);
    unset($_SESSION['password2']);
    unset($_SESSION['passerror2']);

    ?>
