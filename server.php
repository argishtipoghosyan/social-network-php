<?php 
session_start();

include 'database.php';
class Controller extends Database{
    function __construct(){
     	parent ::__construct();
     	if($_SERVER["REQUEST_METHOD"] == 'POST'){
            if(isset($_POST['action'])){
                call_user_func([$this,$_POST['action']]);   
            }
     		if(isset($_POST['signup'])){
     			$this->signup();
     		}
     		if(isset($_POST['signin'])){
     			$this->signin();
     		}
            if(isset($_POST['update'])){
                $this->update();
            }
            if(isset($_POST['updatepas'])){
                $this->updatepas();
            }
            if(isset($_FILES['image-profile'])){
                $this->imageProfile();
            }
            if(isset($_FILES['imagepro'])){
                $this->nkarner();
            }
            if(isset($_POST['status_av'])){
                $this->status();
            }
     	if(isset($_POST['stat_like'])){
                $this->stat_like();
            }}
    }
    function nkarner(){
        $folder_name ='images/'. $_SESSION['user']['name'].$_SESSION['user']['surname'].$_SESSION['user']['id'];
        $image = $_FILES['imagepro'];
        if(!file_exists($folder_name)){
            mkdir($folder_name);
        }
        $hasce =  $folder_name."/".uniqid().$image['name'];
        move_uploaded_file($image['tmp_name'],$hasce);
        $a = $_SESSION['user']['id'];
        $this->db->query("INSERT  INTO images (`name`,`user_id`) VALUES ('$hasce', $a )");
        header('location:photo.php');
    }
    
    function ajax1(){
        $k = $_SESSION['user']['id'];
        $y = $this->db->query("SELECT * FROM images WHERE user_id=$k")->fetch_all(true);
        print json_encode($y);
    }

    function ajax2(){
        $id = $_POST['id'];
        $anun = $_POST['anun'];
        $my_id = $_SESSION['user']['id'];
        unlink($anun);
        if($anun==$_SESSION['user']['image']){
            $_SESSION['user']['image']='images/avatar.png';
        $this->db->query("UPDATE user SET image = 'images/avatar.png' WHERE id = $my_id ");

        }
        $this->db->query("DELETE FROM images WHERE id=$id");

    }
    function ajax3(){
        $a = $_SESSION['user']['id'];
        $id = $_POST['id'];
        $y = $this->db->query("SELECT * FROM images WHERE id=$id")->fetch_all(true);
        $v = $y[0]['name'];
        $this->db->query("UPDATE user SET image = '$v' WHERE id = $a ");
        $_SESSION['user']['image'] = $v;

    }
    function friends(){
        $a = $_SESSION['user']['id'];
        $x = $this->db->query("SELECT * FROM `user` WHERE id IN
             (SELECT user1_id FROM friends WHERE user2_id = $a) or id IN
             (SELECT user2_id FROM friends WHERE user1_id = $a)")->fetch_all(true);
        print json_encode($x);
    }
    function request(){
        $a = $_SESSION['user']['id'];
        $x = $this->db->query("SELECT count(*) FROM request WHERE user2_id = $a")->fetch_all(true);
        print ($x[0]['count(*)']);
        
    }
    function request1(){
        $a = $_SESSION['user']['id'];
        $y = $this->db->query("SELECT * FROM `user` WHERE id IN
             (SELECT user1_id FROM request WHERE user2_id = $a)")->fetch_all(true);
        print json_encode($y);
    }
    function avelacnel(){
        $a = $_SESSION['user']['id'];
        $id = $_POST['id'];
        $this->db->query("DELETE FROM request WHERE user1_id=$id");
        $this->db->query("INSERT INTO friends(user1_id,user2_id) VALUES ($id,$a)");
    }
    function jnjel(){
        $id = $_POST['id'];
        $this->db->query("DELETE FROM request WHERE user1_id=$id");
    }
    function ynker(){
        $a = $_SESSION['user']['id'];
        $id = $_POST['id'];
        $this->db->query("INSERT INTO request (user1_id,user2_id) VALUES ($a,$id)");
    }

    function hetvercnel(){
        $id = $_POST['id'];
        $this->db->query("DELETE FROM request WHERE user2_id=$id");
    }
    function heracnel(){
        $id = $_POST['id'];
        $this->db->query("DELETE FROM friends WHERE user1_id=$id or user2_id=$id");
    }
    function search(){
        $x = $_POST['x'];
        $id = $_SESSION['user']['id'];
        $y=[];
        $arr = $this->db->query("SELECT * FROM `user` WHERE `name` LIKE '$x%' OR `surname` LIKE '$x%'")->fetch_all(true);
        foreach ($arr as $k) {
            $fr = $this->db->query("SELECT * FROM friends WHERE (user1_id=$id and user2_id=$k[id]) or(user1_id=$k[id] and user2_id=$id)")->fetch_all(true);
            $re = $this->db->query("SELECT * FROM request WHERE (user1_id=$id and user2_id=$k[id]) or(user1_id=$k[id] and user2_id=$id)")->fetch_all(true);

            if(empty($fr)){
                $k['status']='enker chi';
            }
            else{
                $k['status']='enker e';

            }
            if(!empty($re)){
                if($re[0]['user1_id']==$id){
                    $k['status']='es em uxarkel';
                }           
                else{         
                    $k['status']='inqn e uxarkel';
                }
            }
            if($k['id']==$id){
                $k['status']='es em';   
            }


            $y[]=$k;
        }
        print json_encode($y);
    }

    function imageProfile(){
        $folder_name = 'images/'.$_SESSION['user']['name'].$_SESSION['user']['surname'].$_SESSION['user']['id'];
        $image = $_FILES['image-profile'];
        // print_r("<pre>");
        print_r($image);
        if(!file_exists($folder_name)){
            mkdir($folder_name);
        }
        $hasce =  $folder_name."/".uniqid().$image['name'];
        move_uploaded_file($image['tmp_name'],$hasce);
        $a = $_SESSION['user']['id'];
        $this->db->query("UPDATE user SET image = '$hasce' WHERE id = $a ");
        $_SESSION['user']['image'] = $hasce;
        header('location:profile.php');
    }
    function signin(){
    	$email1 = $_POST['email1'];
    	$password1 = $_POST['password1'];
    	$x = $this->db->query("SElECT * FROM user WHERE email='$email1'")->fetch_all(true);
    	// print_r($x[0]['password']);
    	// print_r(!$y);
        $_SESSION['login']='checked';
        unset($_SESSION['signup']);
    	if(empty($email1) || empty($x) || empty($password1)){
            if(empty($email1)){
                $_SESSION['email1_error'] = '`EMAIL` դաշտը լրացված չէ';
            }
            else if(empty($x)){
                $_SESSION['email1_error'] = 'Տվյալ`EMAIL`-ով չկա գրանցում';                    
            }
            else{
                $_SESSION['email1'] = $email1;
            }
            if(empty($password1)){
                $_SESSION['password1_error'] = '`PASSWORD` դաշտը լրացված չէ';
            }
            header('location:index.php');
    	} 
    	else if(password_verify($password1, $x[0]['password'])!=1){
            $_SESSION['password1_error'] = '`PASSWORD`-ը սխալ է';
    		header('location:index.php');
    	}
    	else{
            $_SESSION['user'] = $x[0];
    		header('location:profile.php');    		
    	}
    }
    function update(){
        $up_name = $_POST['up_name'];
        $up_surname = $_POST['up_surname'];
        $up_age = $_POST['up_age'];
        $up_email = $_POST['up_email'];
        $a = $_SESSION['user']['id'];
        $y = $this->db->query("SElECT * FROM user WHERE email='$up_email' and id!=$a")->fetch_all(true);
        if(empty($up_name) || empty($up_surname) || empty($up_age) || !filter_var($up_age, FILTER_VALIDATE_INT) || empty($up_email) || !filter_var($up_email, FILTER_VALIDATE_EMAIL) || !empty($y)){
            if(empty($up_name)){
                $_SESSION['up_name_error'] = '`NAME` դաշտը լրացված չէ';
            }
            else{
                $_SESSION['up_name'] = $up_name;
            }
            if(empty($up_surname)){
                $_SESSION['up_surname_error'] = '`SURNAME` դաշտը լրացված չէ';
            }
            else{
                $_SESSION['up_surname'] = $up_surname;
            }
            if(empty($up_email)){
                $_SESSION['up_email_error'] = '`EMAIL` դաշտը լրացված չէ';
            }
            else if(!filter_var($up_email, FILTER_VALIDATE_EMAIL) ){
                $_SESSION['up_email_error'] = 'Չի պարունակում `EMAIL` դաշտ';
            }
            else if(!empty($y)){
                $_SESSION['up_email_error'] = 'Տվյալ`EMAIL`-ով կա գրանցում';                    
            }
            else{
                $_SESSION['up_email'] = $up_email;
            }
            if(empty($up_age)){
                $_SESSION['up_age_error'] = '`AGE` դաշտը լրացված չէ';
            }
            else if(!filter_var($up_age, FILTER_VALIDATE_INT) ){
                $_SESSION['up_age_error'] = 'Թիվ չէ';
            }
            else{
                $_SESSION['up_age'] = $up_age;
            }
            header('location:update.php');
        }
        else{
            // $id = $this->db->insert_id;
            $this->updat('user',['name'=>$up_name,'surname'=>$up_surname,'age'=>$up_age,'email'=>$up_email],['id'=> $a]);
        $x = $this->db->query("SElECT * FROM user WHERE id='$a'")->fetch_all(true);
                $_SESSION['user']=$x[0];
            header('location:update.php');
            // print_r($_SESSION['up_name']);
        }
    }
    function updatepas(){
        $password = $_POST['password'];
        $password1 = $_POST['password1'];
        $password2 = $_POST['password2'];
        $a = $_SESSION['user']['password'];
        $b = $_SESSION['user']['id'];
        print $a;
        if(empty($password) ||empty($password1) ||empty($password2) || password_verify($password, $a)!=1 || strlen($password1)<6 || strlen($password1)>12){
            if(strlen($password1)<6){
                $_SESSION['passerror1'] = '`PASSWORD`ի սինվոլների քանակը քիչ է 6- ից';
            }
            else if(strlen($password1)>12){
                $_SESSION['passerror1'] = '`PASSWORD`-ի սինվոլների քանակը շատ է 12- ից ';
            }
            if(empty($password)){
                $_SESSION['passerror'] = '`PASSWORD` դաշտը լրացված չէ';
            }
            if(empty($password1)){
                $_SESSION['passerror1'] = '`PASSWORD` դաշտը լրացված չէ';
            }
            if(empty($password2)){
                $_SESSION['passerror2'] = '`PASSWORD` դաշտը լրացված չէ';
            }
            else if( $password1!=$passerror2){
                $_SESSION['passerror1'] = 'Նույնը չեն';
            }
            if(password_verify($password, $a)!=1){
                $_SESSION['passerror'] = '`PASSWORD` դաշտը սխալ է';   
            }              
            header('location:update.php'); 
        }
        else{
            $password1 = password_hash($password1, PASSWORD_DEFAULT);
            $this->updat('user',['password'=>$password1],['id'=> $b]);
            header('location:update.php');
        }
    }

    function signup(){
    	$name = $_POST['name'];
    	$surname = $_POST['surname'];
    	$age = $_POST['age'];
    	$password = $_POST['password'];
    	$repeat_password = $_POST['repeat_password'];
    	$email = $_POST['email'];
    	$data = $this->db->query("SElECT * FROM user where email='$email'")->fetch_all(true);
        $_SESSION['signup']='checked';
        unset($_SESSION['login']);

    	if(empty($name) || empty($surname) || empty($age) || !filter_var($age, FILTER_VALIDATE_INT) || empty($email) || empty($password) || empty($repeat_password) || $password!=$repeat_password || !filter_var($email, FILTER_VALIDATE_EMAIL) || !empty($data) || strlen($password)>12 || strlen($password)<6){
           	if(empty($name)){
                $_SESSION['name_error'] = '`NAME` դաշտը լրացված չէ';
            }
            else{
            	$_SESSION['name'] = $name;
            }
            if(empty($surname)){
                $_SESSION['surname_error'] = '`SURNAME` դաշտը լրացված չէ';
            }
            else{
            	$_SESSION['surname'] = $surname;
            }
            if(empty($email)){
                $_SESSION['email_error'] = '`EMAIL` դաշտը լրացված չէ';
            }
            else if(!filter_var($email, FILTER_VALIDATE_EMAIL) ){
                $_SESSION['email_error'] = 'Չի պարունակում `EMAIL` դաշտ';
            }
            else if(!empty($data)){
                $_SESSION['email_error'] = 'Տվյալ`EMAIL`-ով կա գրանցում';                    
            }
            else{
                $_SESSION['email'] = $email;
            }
            if(empty($age)){
                $_SESSION['age_error'] = '`AGE` դաշտը լրացված չէ';
            }
            else if(!filter_var($age, FILTER_VALIDATE_INT) ){
                $_SESSION['age_error'] = 'Թիվ չէ';
            }
            else{
                $_SESSION['age'] = $age;
            }
            if(strlen($password)<6){
            	$_SESSION['password_error'] = '`PASSWORD`ի սինվոլների քանակը քիչ է 6- ից';
            }
            else if(strlen($password)>12){
            	$_SESSION['password_error'] = '`PASSWORD`-ի սինվոլների քանակը շատ է 12- ից ';
            }
            if(empty($password)){
                $_SESSION['password_error'] = '`PASSWORD` դաշտը լրացված չէ';
            }
            else{
                $_SESSION['password'] = $password;
            }
            if(empty($repeat_password)){
                $_SESSION['repeat_password_error'] = '`REPEAT PASSWORD` դաշտը լրացված չէ';
            }
            else if( $password!=$repeat_password){
                $_SESSION['repeat_password_error'] = 'Նույնը չեն';
            }
            else{
                $_SESSION['repeat_password'] = $repeat_password;
            }

        header('location:index.php');
    	}
        else {
        	$password = password_hash($password, PASSWORD_DEFAULT);
        	$this->insert('user',['name'=>$name,'surname'=>$surname,'age'=>$age,'email'=>$email,'password'=>$password]);
    	    $id = $this->db->insert_id;
            $arr = $this->db->query("SELECT * FROM user WHERE id=$id")->fetch_all(true);
    	    $_SESSION['user'] = $arr[0];
        	header('location:profile.php');
        }
    } 
    function friendsMessage(){
        $a = $_SESSION['user']['id'];
        $arr = $this->db->query("SELECT * FROM `user` WHERE id IN
             (SELECT user1_id FROM friends WHERE user2_id = $a) or id IN
             (SELECT user2_id FROM friends WHERE user1_id = $a)")->fetch_all(true);
        $x=[];

        foreach ($arr as $key ) {
            $count = $this->db->query("SELECT count(*) as count FROM message WHERE user2_id = $a and user1_id=$key[id] and status=1")->fetch_all(true);
            $key['namakner']=$count[0]['count'];
            $x[]=$key;
        }
        print json_encode($x);
    }
    function chkardacvac(){
        $a = $_SESSION['user']['id'];
        $x = $this->db->query("SELECT sum(status) FROM message WHERE   user2_id = $a and status=1")->fetch_all(true);
        print ($x[0]['sum(status)']);
    }
   
    function namakner(){
        $id = $_POST['id'];
        $o = $_POST['o'];
        $a = $_SESSION['user']['id'];
        $namakner = $this->db->query("SELECT * FROM `message` WHERE (user1_id = $a AND user2_id = $id) OR (user1_id = $id AND user2_id = $a)")->fetch_all(true);
        print json_encode($namakner);
        $this->db->query("UPDATE  `message` SET status = $o WHERE (user1_id = $id AND user2_id = $a)");

    }
    function messag(){
        $namak = $_POST['namak'];
        $a = $_SESSION['user']['id'];
        $id = $_POST['id'];
        if(!empty($namak)){
        $this->insert('message',['user1_id'=>$a,'user2_id'=>$id,'message'=>$namak])->fetch_all(true);
        header('location:messaging.php');
        }
    }
    

    function status(){
        $folder_name ='images/'. $_SESSION['user']['name'].$_SESSION['user']['surname'].$_SESSION['user']['id'];
        $image = $_FILES['imagestat'];
        if(!file_exists($folder_name)){
            mkdir($folder_name);
        }
        $hasce =  $folder_name."/".uniqid().$image['name'];
        move_uploaded_file($image['tmp_name'],$hasce);

        $a = $_SESSION['user']['id'];
        $user_id = $_SESSION['user']['id'];  
        $status = $_POST['status'];
        if(!empty($status) && !empty($image['name'])){        
            $this->insert('status',['status'=>$status,'user_id'=>$user_id,'image'=>$hasce]);
        }
        else if(!empty($status) && empty($image['name'])){
            $this->insert('status',['status'=>$status,'user_id'=>$user_id]);
        }
        else if(empty($status) && !empty($image['name'])){        
            $this->insert('status',['image'=>$hasce,'user_id'=>$user_id]);
        }
        header('location:profile.php');
    }

    function news(){
        $a = $_SESSION['user']['id'];
        $y=[];
        $x = $this->db->query("SELECT * FROM user JOIN STATUS on user.id = status.user_id WHERE  user.id != $a ORDER BY tyme DESC" )->fetch_all(true);
        foreach ($x as $k) {
            $c = $k['id'];
            $like = $this->db->query("SELECT * FROM `like` WHERE user_id=$a and status_id=$c ")->fetch_all(true);
            $fr = $this->db->query("SELECT * FROM friends WHERE (user1_id=$a and user2_id=$k[user_id]) or(user1_id=$k[user_id] and user2_id=$a)")->fetch_all(true);
            if(empty($like)){
                $k['like'] = 'like';
            }
            else{   
                $k['like'] = 'nolike';
            }
            if(empty($fr)){
                $k['stat'] = 'enker chi';

            }
            else{
                $k['stat']='enker e';

            }
        $y[]=$k;
        }
        
        print json_encode($y);        
    }
    function commentner(){
        $status_id = $_POST['status_id'];
        // print_r($status_id);
        $a = $_SESSION['user']['id'];
        $y = [];
        $commentner = $this->db->query("SELECT * FROM user JOIN comment on user.id = comment.user_id  where comment.status_id = $status_id ORDER BY tyme ASC")->fetch_all(true);
        foreach ($commentner as $k) {
            if($k['user_id'] == $a){
                $k['ogtater'] = '0';
            }
            else{
                $k['ogtater'] = '1';                
            }
            $y[]=$k;

        }

        print json_encode($y);
    }
    function stat_like(){
        $user_id = $_SESSION['user']['id'];
        $status_id = $_POST['id'];
        $x = $this->db->query("SELECT * FROM `like` WHERE user_id=$user_id and status_id=$status_id")->fetch_all(true);
        print json_encode($x); 

        if(!empty($x)){
            $this->db->query("DELETE FROM `like` WHERE user_id=$user_id and status_id=$status_id"); 
        }
        else {
            $this->db->query("INSERT INTO `like`(user_id,status_id) VALUES ($user_id,$status_id)");
        }
    }
    function comment(){
        $status_id = $_POST['status_id'];
        $a = $_SESSION['user']['id'];
        $comment = $_POST['commen'];
        if(!empty($comment)){ 
        $this->insert('comment',['user_id'=>$a,'status_id'=>$status_id,'comment'=>$comment])->fetch_all(true);
        header('location:profile.php');
        }
    }
}
new Controller();
?>
